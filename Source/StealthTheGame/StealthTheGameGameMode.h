// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StealthTheGameGameMode.generated.h"

UCLASS(minimalapi)
class AStealthTheGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AStealthTheGameGameMode();
};



